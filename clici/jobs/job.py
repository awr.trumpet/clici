"""Base class for jobs used by clici."""
import click
from docker.models.containers import Container
from clici.containers import run_container

class Job:
    """
    Base class for defining a single job which will run in docker.

    Args:
        name (str): This is the name of the job.
        docker_image (str): This is the docker image that you want to run your scripts in.
        script (str): This is the main testing script you run for your CI job.

    Attributes:
        image (str): The docker image needed for this job.
        script (str): The main CI script to run for this job.

    Methods:
        run: This runs the specific job
    """
    def __init__(self, name: str, docker_image: str, script: str):
        self.name = name
        self._image = docker_image
        self._script = script

    @property
    def image(self) -> str:
        """Gets the docker image that the job needs to run."""
        return self._image

    @property
    def script(self) -> str:
        """Gets the main CI script to run in the docker image."""
        return self._script

    def run(self, container: Container):
        """
        Run this particular job in the given docker container.

        Args:
            container (Container): The container to run this
                job in. The container must have already been
                prepared with everything needed for the job.
        """
        script_error = self._run_script(container)
        return script_error

    def _run_script(self, container: Container) -> int:
        """
        Function to run the main CI script from a given job.
        This will run the script in the container and print the output
        of the the script.

        Args:
            container (Container): The docker container to run the script in.
            job (Job): The job instance to run the script on.

        Returns:
            int: The error code from running the script.
        """
        stderr, stdout = run_container(container, self.script)
        click.echo(stdout)
        return stderr
