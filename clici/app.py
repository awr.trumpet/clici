"""Main entry point to the clici command line interface"""
from pathlib import Path
import click
from clici.yaml_parsing import parse_file
from clici.ci_files import get_ci_file


@click.command()
@click.option('-f', '--file', 'file_path', default=None,
    type=click.Path(exists=True, dir_okay=True), help=('the path to the particular CI file. '
    'If given a directory then it looks in that directory for a supported CI file. '
    'If no path is given then the current working directory is used.'))
@click.option('-j', '--job', 'job_names', multiple=True, help=('specify a particular job to run '
    'from within the CI file. Can have multiple jobs and it will run only the jobs given. '
    'By default all jobs will be run.'))
@click.option('-r', 'remove_on_fail', is_flag=True, type=bool, default=False, help=('specify '
    'whether to remove the docker containers, even if the job fails.'))
def cli(file_path: str, job_names: tuple, remove_on_fail: bool):
    """
    clici command line.

    Args:
        file_path (str, Optional): The path to a CI file. It must be one of the supported CI file
            types. If a directory is passed it looks in that directory for a supported CI file. If
            nothing is passed then it looks in the current working directory for a supported file.
            Note the directory that the file is in is sent to the docker containers since its
            assumed this is the root of your project.
        job_names (tuple, Optional): A tuple of strings containeing job names to run from the
            pipeline. If not specified it runs all of the jobs in the pipeline.
        remove_on_fail (bool, Optional): Specify whether the containers should be removed if the
            scripts fail. By default the containers are stopped but not removed so you can re-enter
            to debug.
    """
    if not file_path:
        file_path = Path.cwd()
    else:
        file_path = Path(file_path)
    clici(file_path, job_names, remove_on_fail)


def clici(file_path: Path=Path.cwd(), job_names: tuple=(), remove_on_fail: bool=False):
    """
    Run clici pipelines from a file.

    Args:
        file_path (Path, Optional): The path to a CI file. It must be one of
            the supported CI file types. If a directory is passed it looks
            in that directory for a supported CI file. If nothing is passed
            then it looks in the current working directory for a supported
            file. Note the directory that the file is in is sent to the
            docker containers since its assumed this is the root of your
            project.
        job_names (tuple, Optional): A tuple of strings containeing job
            names to run from the pipeline. If not specified it runs all of
            the jobs in the pipeline.
        remove_on_fail (bool, Optional): Specify whether the containers
            should be removed if the scripts fail. By default the containers
            are stopped but not removed so you can re-enter to debug.
    """
    ci_file = get_ci_file(file_path)
    pipeline = parse_file(ci_file)
    pipeline.test_stage.run(job_names, ci_file.parent, remove_on_fail)
