"""Derived GitLab pipeline class for creating pipelines from gitlab CI."""
from docker import from_env
from clici.pipelines.pipeline import Pipeline
from clici.stages import TestStage
from clici.jobs import Job


class GitLabPipeline(Pipeline):
    """
    Class for creating a pipeline from a .gitlab-ci.yml file.
    Args:
        name (str): This is the name of the pipeline.
        yaml_dictionary (dict): The dictionary returned from parsing the GitLab yaml file.
    """
    @classmethod
    def from_dictionary(cls, dictionary):
        docker_client = from_env()
        jobs = [
            Job(job_name, attributes['image'], attributes['script'])
            for job_name, attributes in dictionary.items()
        ]
        test_stage = TestStage(jobs, docker_client)
        return cls(test_stage)
