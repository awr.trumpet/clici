"""Base class for pipelines used by clici."""
from clici.stages import TestStage


class Pipeline:
    """
    Base class for defining a single pipeline which will run in docker.

    Args:
        test_stage (TestStage): The test stage instance for this pipeline.

    Attributes:
        test_stages (TestStage): Gets the test stage class
        jobs (list): Gets a list of jobs in all the stages.
    """
    def __init__(self, test_stage: TestStage):
        self._test_stage = test_stage

    @property
    def test_stage(self) -> TestStage:
        """Gets list of all tests stages in this pipeline."""
        return self._test_stage

    @classmethod
    def from_dictionary(cls, dictionary: dict):
        """
        Overloadable class method to create a pipeline class from
        a dictionary. Primarily used for creating pipeline classes
        from yaml files. The implementation needs to be written
        for each class that inherits from this class.

        Args:
            dictionary (dict): The raw dictionary received from
                reading the yaml file.
        """
        raise NotImplementedError
