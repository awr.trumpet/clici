"""Global dictionary of supported file name and their associated pipeline."""
from clici.pipelines.gitlab_pipeline import GitLabPipeline


SUPPORTED_PIPELINES = {
    '.gitlab-ci.yml': GitLabPipeline
}
