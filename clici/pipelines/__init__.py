"""API import for the various pipelines."""
from clici.pipelines.pipeline import Pipeline
from clici.pipelines.gitlab_pipeline import GitLabPipeline
from clici.pipelines.supported_pipelines import SUPPORTED_PIPELINES
