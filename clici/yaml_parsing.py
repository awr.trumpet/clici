"""Functions for parsing different CI yaml files."""
from pathlib import Path
from yaml import safe_load
from clici.pipelines import Pipeline, SUPPORTED_PIPELINES


def parse_yaml(yaml_string: str, pipeline_class: Pipeline) -> Pipeline:
    """
    Function to parse a yaml string and convert it into
    a clici `Pipeline` instance.

    Args:
        yaml_string (str): The yaml string to parse.
        pipeline_class (Pipeline): The type of pipeline
            to interpret the `yaml_string` as.

    Returns:
        A list of ```Pipeline``` instances.
    """
    yaml_dictionary = safe_load(yaml_string)
    return pipeline_class.from_dictionary(yaml_dictionary)


def parse_file(file_path: Path) -> list:
    """
    Function to parse a yaml file into a clici pipeline.

    Args:
        file_path (Path): Path to the yaml file to parse.

    Returns:
        A list of ```Pipeline``` instances.
    """
    with file_path.open('r') as yaml_file:
        return parse_yaml(yaml_file, SUPPORTED_PIPELINES[file_path.name])
