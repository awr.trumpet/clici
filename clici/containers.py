"""Functions for interacting with docker containers."""
from tarfile import TarFile
from io import BytesIO
from pathlib import Path
from docker import DockerClient
from docker.models.containers import Container


def create_container(
    docker_client: DockerClient,
    docker_image: 'str',
    container_name: 'str'=None) -> Container:
    """
    Function to create and run a container. The container should
    stay running by running a command which keeps the
    container open in the background.

    Args:
        docker_client: The instance of the docker client to interact
            with the docker daemon.
        docker_image (str): The docker image to start the container
            on.
        container_name (str): The name of the container. If not
            specified this is determined by docker.

    Returns:
        Returns a `Container` instance.
    """
    return docker_client.containers.run(
        docker_image,
        ['tail', '-f', '/dev/null'],
        name=container_name,
        detach=True)


def remove_container(docker_container: Container) -> None:
    """
    Function to force remove a docker container.

    Args:
        docker_container: The docker container to remove.
    """
    docker_container.remove(force=True)


def stop_container(docker_container: Container) -> None:
    """
    Function to stop a docker container.

    Args:
        docker_container: The docker container to stop.
    """
    docker_container.stop(timeout=0)


def run_container(docker_container: Container, command) -> (int, bytes):
    """
    Function to run a command in a running docker container.

    Args:
        docker_container: The docker container to run in.
        command: The docker command to run in the container.

    Returns:
        Tuple of ```(int, bytes)```, the exit code of the
            command and anything send to stdout.
    """
    return docker_container.exec_run(command)


def copy_to_container(container: Container, source_directory: Path, container_directory: Path):
    """
    Copies an entire directory to the the container.

    Args:
        container (Container): The container you want
            to copy directory into.
        source_directory (Path): The directory you want
            to copy to the container.
        container_directory (Path): The directory in
            the container you want to copy to
    """
    tar_stream = BytesIO()
    with TarFile(fileobj=tar_stream, mode='w') as tar_file:
        tar_file.add(source_directory, arcname=source_directory.name)
    tar_stream.seek(0)
    container.put_archive(container_directory, tar_stream)
