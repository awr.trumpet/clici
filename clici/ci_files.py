"""Functions for finding and returning CI files"""
from pathlib import Path
from clici.pipelines import SUPPORTED_PIPELINES


def get_ci_file(file_path: Path) -> Path:
    """
    Given a path locate a supported CI yaml file. If `file_path`
    is a directory then it looks in that directory for a supported
    yaml file. If `file_path` is a file it checks the file is
    supported and then returns that file path.

    Args:
        file_path (Path): The starting path to look for the CI files.

    Returns:
        `Path` object to the CI file.

    Raises:
        `FileNotFoundError` if the file is not supported or no supported
            file can be found.
    """
    if file_path.is_dir():
        ci_file = find_ci_file(file_path)
    else:
        file_name = file_path.name
        if file_name in SUPPORTED_PIPELINES:
            ci_file = file_path
        else:
            raise FileNotFoundError(f'{file_name} is not a supported file name.')
    return ci_file


def find_ci_file(directory: Path) -> Path:
    """
    Looks in the current working directory for any of the supported filenames.

    Args:
        directory (Path): The directory to look in.

    Returns:
        Path instance to the CI file.

    Raises:
        `FileNotFoundError` if no supported yaml files are found in the given
            directory.
    """
    for yaml_file in SUPPORTED_PIPELINES:
        if (directory / yaml_file).exists():
            return directory / yaml_file
    raise FileNotFoundError(f'Could not find any supported yaml files in {directory}.')
