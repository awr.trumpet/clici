"""Base class for a clici stage which containers jobs."""
from pathlib import Path
from docker import DockerClient
from clici.containers import (create_container, copy_to_container,
    remove_container, stop_container)


class Stage:
    """
    Base class for creating a general clici stage. This
    should contain all the jobs relevant to this stage
    and the TestStage and BuildStage classes will
    inherit from this class.

    Args:
        jobs (list): A list of `Job` instances which are
            relevant to this stage.
    """
    def __init__(self,  jobs: list, docker_client: DockerClient):
        self.docker_client = docker_client
        self._jobs = jobs
        self._job_dictionary = {job.name: job for job in jobs}

    @property
    def jobs(self) -> list:
        """The list of jobs in this stage."""
        return self._jobs

    def __getitem__(self, job_name: str):
        """Return a job based on the job name."""
        return self._job_dictionary[job_name]

    def jobs_to_run(self, job_names: tuple) -> list:
        """
        Given a tuple of job names return a list of job
        instances from this stage.

        Args:
            job_names (tuple): Tuple of strings of specific
                jobs to run from this stage.

        Returns:
            (list): list of job instances.
        """
        if not job_names:
            jobs_to_run = self.jobs
        else:
            jobs_to_run = [job for job in self.jobs if job.name in job_names]
        return jobs_to_run

    def run(self,
        job_names: tuple=None,
        source_directory: Path=None,
        remove_on_fail: bool=False):
        """
        Run the given job names from this stage and remove
        them if `remove_on_fail` is `True`.

        Args:
            job_names (tuple): Tuple of strings of specific
                jobs to run from this stage.
            source_directory (Path): The path to the source
                code to copy into the docker container.
            remove_on_fail (bool): If set to true the
                container will be deleted, even if the job
                fails.
        """
        container_directory = Path('/tmp')
        for job in self.jobs_to_run(job_names):
            container = create_container(self.docker_client, job.image, job.name)
            if source_directory:
                copy_to_container(container, source_directory, container_directory)
            job_error = job.run(container)
            if job_error == 0 or remove_on_fail:
                remove_container(container)
            else:
                stop_container(container)
