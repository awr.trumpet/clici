"""Base clici test stage."""
from clici.stages.stage import Stage


class TestStage(Stage):
    """
    Test stage class for creating a clici test stage
    which contains all the test jobs to run.
    """
    __test__ = False
