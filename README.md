# Welcome to the clici project (Command Line Interface Continuous Integration)!
The aim of this project is to make it possible to run multiple formats of Continous Integration (CI) on a local machine. Most commonly used CI platforms use YAML files to control how to interface with Docker. This file details what Docker image to pull, how to install your script and how to run your tests.

**Note: This package does not install the docker engine and requries the docker engine to be installed on the host machine to work. Please refer to the [Docker](https://docs.docker.com/engine/install/) website for how to install on your machine.**

These current CI platforms are supported:
 - [GitLab](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
 - [Circle](https://circleci.com/)
 - [Travis](https://travis-ci.org/)

### Coding API
To make a new standard you must create a new pipeline object which inherits from the main `Pipeline` Abstract Base Class. This base class has all the needed properties and attributes and you should convert the new YAML syntax into this "pseudo-syntax" as appropriate. The main properties are:
 - **image**: This is the Docker image that you want to run on.
 - **pre_scripts**: A list of  commands to run before your main script. This will detail any requirements needed for the scripts and how to install your code.
 - **script**: This is the main script you want to run, usually the command to run your tests.
 - **post_scripts**: A list of commands to run after the main script has run.