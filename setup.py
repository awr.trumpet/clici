from setuptools import setup, find_packages
from pathlib import Path


with Path('README.md').open() as readme:
    long_description = readme.read()


setup(
    name='clici',
    author='Adam Richardson',
    author_email='awr.trumpet@gmail.com',
    description='Package to run CI YAML files using docker',
    url='https://gitlab.com/awr.trumpet/clici',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=find_packages(exclude=['*tests']),
    install_requires=[
        'docker',
        'pyyaml',
        'click'
    ],
    entry_points={
        'console_scripts': [
            'clici=clici.app:cli'
        ]
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Operating System :: OS Independent'
    ],
    python_requires='!=3.0,!=3.1,!=3.2,!=3.3,!=3.4',
    use_scm_version=True
)