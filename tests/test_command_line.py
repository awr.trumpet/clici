import pytest
from clici.app import cli


@pytest.mark.docker
def test_if_no_additional_arguments_are_passed(cli_runner, yaml_file):
    yaml = yaml_file('one_job_yamls', '.gitlab-ci.yml')
    result = cli_runner.invoke(cli, ['-f', str(yaml)])
    assert result.exit_code == 0
    assert result.output == 'hello world\n\n'


@pytest.mark.docker
@pytest.mark.parametrize('job_name', [
    'job1',
    'job2'
])
def test_if_a_job_name_is_given_it_only_runs_that_job(cli_runner, yaml_file, job_name):
    yaml = yaml_file('two_job_yamls', '.gitlab-ci.yml')
    result = cli_runner.invoke(cli, ['-f', str(yaml), '-j', job_name])
    assert result.exit_code == 0
    assert result.output == f'{job_name}\n\n'


@pytest.mark.docker
def test_giving_both_job_names_runs_both_jobs_in_the_pipeline(cli_runner, yaml_file):
    yaml = yaml_file('two_job_yamls', '.gitlab-ci.yml')
    result = cli_runner.invoke(cli, ['-f', str(yaml), '-j', 'job1', '-j', 'job2'])
    assert result.exit_code == 0
    assert result.output == 'job1\n\njob2\n\n'