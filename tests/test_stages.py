from docker.errors import NotFound
import pytest
from clici.jobs.job import Job
from clici.stages.test_stage import TestStage
from clici.containers import remove_container


@pytest.fixture(scope='session')
def alpine_jobs():
    return [
        Job('job1', 'alpine', 'echo job1'),
        Job('job2', 'alpine', 'echo job2'),
        Job('job3', 'alpine', 'echo job3')
    ]


@pytest.fixture
def good_test_stage(docker_client, alpine_jobs):
    return TestStage(alpine_jobs, docker_client)


@pytest.fixture
def bad_test_stage(docker_client):
    bad_job = Job('bad_job', 'alpine', 'exit 1')
    test_stage = TestStage([bad_job], docker_client)
    yield test_stage
    try:
        remove_container(get_container(docker_client, test_stage['bad_job']))
    except NotFound:
        pass


def get_container(docker_client, job):
    return docker_client.containers.get(job.name)


class TestTestStagesContainerHandling:
    def test_running_a_test_stage_deletes_the_container_if_the_script_passes(self, docker_client, good_test_stage):
        good_test_stage.run(('job1'))
        with pytest.raises(NotFound):
            get_container(docker_client, good_test_stage['job1'])

    def test_running_a_test_stage_doesnt_delete_the_container_if_the_script_fails(self,bad_test_stage, docker_client):
        bad_test_stage.run()
        container = get_container(docker_client, bad_test_stage['bad_job'])
        assert container in docker_client.containers.list(all=True)

    def test_running_a_bad_job_stops_the_container_by_default(self, docker_client, bad_test_stage):
        bad_test_stage.run()
        container = get_container(docker_client, bad_test_stage['bad_job'])
        assert container.status == 'exited'

    def test_running_a_bad_job_with_remove_on_fail_removes_the_container(self, docker_client, bad_test_stage):
        bad_test_stage.run(('job1'), remove_on_fail=True)
        with pytest.raises(NotFound):
            get_container(docker_client, bad_test_stage['bad_job'])


@pytest.mark.docker
class TestTestStageJobsRunning:
    def test_not_specifying_specific_jobs_runs_all_the_jobs(self, capsys, good_test_stage):
        good_test_stage.run()
        assert capsys.readouterr().out == 'job1\n\njob2\n\njob3\n\n'

    @pytest.mark.parametrize('job_name', [
        'job1',
        'job2',
        'job3'
    ])
    def test_giving_a_specific_job_name_runs_just_that_job(self, capsys, good_test_stage, job_name):
        good_test_stage.run((job_name))
        assert capsys.readouterr().out == f'{job_name}\n\n'

    def test_giving_two_jobs_just_runs_the_specified_two(self, capsys, good_test_stage):
        good_test_stage.run(('job1, job3'))
        assert capsys.readouterr().out == 'job1\n\njob3\n\n'

    def test_giving_a_job_name_not_in_the_list_of_all_jobs_doesnt_put_anything_on_stdout(self, capsys, good_test_stage):
        good_test_stage.run(('blah'))
        assert capsys.readouterr().out == ''