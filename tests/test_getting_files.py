import pytest
from clici.ci_files import get_ci_file, find_ci_file


def test_finding_a_supported_yaml_file_in_a_directory_returns_the_correct_path(tmp_path):
    gitlab_yaml = tmp_path / ".gitlab-ci.yml"
    gitlab_yaml.touch()
    assert find_ci_file(tmp_path) == gitlab_yaml


def test_trying_to_find_a_file_in_an_empty_directory_raises_an_error(tmp_path):
    with pytest.raises(FileNotFoundError):
        find_ci_file(tmp_path)


def test_getting_a_ci_file_when_given_a_supported_file_returns_the_correct_path(tmp_path):
    gitlab_yaml = tmp_path / ".gitlab-ci.yml"
    gitlab_yaml.touch()
    assert get_ci_file(gitlab_yaml) == gitlab_yaml


def test_getting_a_ci_file_when_given_a_not_supported_file_raises_an_error(tmp_path):
    bad_yaml = tmp_path / "bad.yml"
    bad_yaml.touch()
    with pytest.raises(FileNotFoundError):
        get_ci_file(bad_yaml)


def test_getting_a_ci_file_when_given_an_empty_directory_raises_an_error(tmp_path):
    with pytest.raises(FileNotFoundError):
        get_ci_file(tmp_path)


def test_getting_a_ci_file_in_a_directory_containing_a_ci_file_returns_the_correct_path(tmp_path):
    gitlab_yaml = tmp_path / ".gitlab-ci.yml"
    gitlab_yaml.touch()
    assert get_ci_file(tmp_path) == gitlab_yaml