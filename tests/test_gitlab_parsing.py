import pytest
from clici.yaml_parsing import parse_file, parse_yaml
from clici.pipelines import GitLabPipeline


@pytest.mark.parametrize('yaml_directory', [
    'one_job_yamls',
    'two_job_yamls'
])
def test_parsing_a_gitlab_yaml_string_creates_a_gitlab_pipeline(yaml_string, yaml_directory):
    pipeline = parse_yaml(yaml_string(yaml_directory, '.gitlab-ci.yml'), GitLabPipeline)
    assert isinstance(pipeline, GitLabPipeline)


@pytest.mark.parametrize('yaml_directory', [
    'one_job_yamls',
    'two_job_yamls'
])
def test_parsing_a_gitlab_yaml_file_creates_a_list_of_gitlab_pipelines(yaml_file, yaml_directory):
    pipeline = parse_file(yaml_file(yaml_directory, '.gitlab-ci.yml'))
    assert isinstance(pipeline, GitLabPipeline)
