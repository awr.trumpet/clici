import pytest
from clici.app import clici


@pytest.mark.docker
def test_running_the_one_job_gitlab_file_puts_the_correct_thing_on_stdout(capsys, yaml_file):
    clici(yaml_file('one_job_yamls', '.gitlab-ci.yml'))
    assert capsys.readouterr().out == 'hello world\n\n'


@pytest.mark.docker
def test_running_the_two_job_gitlab_file_puts_the_correct_thing_on_stdout(capsys, yaml_file):
    clici(yaml_file('two_job_yamls', '.gitlab-ci.yml'))
    assert capsys.readouterr().out == 'job1\n\njob2\n\n'


@pytest.mark.docker
def test_selecting_only_one_job_only_runs_that_job(capsys, yaml_file):
    file = yaml_file('two_job_yamls', '.gitlab-ci.yml')
    clici(file, ('job1'))
    assert capsys.readouterr().out == 'job1\n\n'


@pytest.mark.docker
def test_selecting_two_jobs_runs_both_jobs(capsys, yaml_file):
    file = yaml_file('two_job_yamls', '.gitlab-ci.yml')
    clici(file, ('job1', 'job2'))
    assert capsys.readouterr().out == 'job1\n\njob2\n\n'
