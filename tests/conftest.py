import pytest
from pathlib import Path
from docker import from_env
from clici.containers import create_container, remove_container

@pytest.fixture(scope='session')
def tests_directory():
    return Path(__file__).parent


@pytest.fixture(scope='session')
def yaml_file(tests_directory):
    def wrapper(directory_name, file_name):
        return tests_directory / directory_name / file_name
    return wrapper


@pytest.fixture(scope='session')
def yaml_string(yaml_file):
    def wrapper(directory_name, file_name):
        return yaml_file(directory_name, file_name).read_text()
    return wrapper


@pytest.fixture(scope='session')
def docker_client():
    return from_env()


@pytest.fixture
def alpine_container(docker_client):
    alpine = create_container(docker_client, 'alpine')
    yield alpine
    remove_container(alpine)