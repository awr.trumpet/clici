import pytest
from clici.jobs.job import Job
from clici.containers import create_container, remove_container


@pytest.fixture
def alpine_job(docker_client):
    return Job('alpine_job', 'alpine', 'echo hello world')


@pytest.fixture(scope='session')
def alpine_container(docker_client):
    container = create_container(docker_client, 'alpine', 'alpine_container')
    yield container
    remove_container(container)


@pytest.mark.docker
class TestJobRunning:
    def test_running_a_job_prints_the_output_of_the_script(self, capsys, alpine_job, alpine_container):
        alpine_job.run(alpine_container)
        assert capsys.readouterr().out == 'hello world\n\n'
