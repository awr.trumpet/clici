import pytest
from pathlib import Path
import docker
from clici.pipelines.pipeline import Pipeline
from clici.containers import (create_container, remove_container,
    stop_container, run_container, copy_to_container)


@pytest.mark.docker
class TestContainerCreation:
    def test_creating_a_container_creates_a_new_container_but_doesnt_start_it(self, docker_client):
        container = create_container(docker_client, 'alpine')
        assert container in docker_client.containers.list(all=True)
        remove_container(container)


    def test_creating_two_containers_and_removing_just_one_of_them_leaves_the_other_in_the_list (self, docker_client):
        container1 = create_container(docker_client, 'alpine')
        container2 = create_container(docker_client, 'alpine')
        remove_container(container1)
        assert container2 in docker_client.containers.list(all=True)
        remove_container(container2)


    def test_creating_a_container_sets_the_container_running(self, docker_client):
        container = create_container(docker_client, 'alpine')
        assert container in docker_client.containers.list(all=False)
        remove_container(container)


@pytest.mark.docker
class TestContainerRunning:
    def test_running_a_bad_command_in_a_container_returns_an_error(self, alpine_container):
        error, _ = run_container(alpine_container, 'blah')
        assert error != 0


    def test_running_a_good_command_in_a_container_returns_zero_exit_code(self, alpine_container):
        error, _ = run_container(alpine_container, 'which sh')
        assert error == 0


    def test_running_a_good_command_returns_the_output_it_should(self, alpine_container):
        _, output = run_container(alpine_container, 'which sh')
        assert output == b'/bin/sh\n'


@pytest.mark.docker
def test_stopping_a_container_gives_its_status_as_exited(docker_client, alpine_container):
    stop_container(alpine_container)
    assert docker_client.containers.get(alpine_container.id).status == 'exited'


@pytest.mark.docker
def test_a_stopped_container_can_be_started_again(docker_client, alpine_container):
    stop_container(alpine_container)
    alpine_container.start()
    assert docker_client.containers.get(alpine_container.id).status == 'running'


@pytest.mark.docker
def test_removing_a_container_removes_the_container_from_the_list(docker_client):
    container = create_container(docker_client, 'alpine')
    remove_container(container)
    assert container not in docker_client.containers.list(all=True)


@pytest.mark.docker
def test_installing_something_in_a_docker_container_is_still_installed_later(alpine_container):
    alpine_container.exec_run('apk add git')
    stderr, stdout = alpine_container.exec_run('which git')
    assert stderr == 0


@pytest.mark.docker
def test_copying_directory_to_a_container_puts_the_contents_of_that_directory_in_the_container(alpine_container, tmp_path):
    content = 'this is a test.'
    file = tmp_path / 'hello.txt'
    file.write_text(content)
    container_directory = Path('/tmp')
    copy_to_container(alpine_container, tmp_path, container_directory)
    error, output = run_container(alpine_container, f'[ -d {container_directory / tmp_path.name} ]')
    assert error == 0
