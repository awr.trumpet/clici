import pytest


@pytest.mark.parametrize('yaml_directory', [
    'one_job_yamls',
    'two_job_yamls'
])
@pytest.mark.parametrize('file_name', [
    '.gitlab-ci.yml'
])
def test_single_pipeline_yamls_are_readable_from_these_tests(yaml_file, yaml_directory, file_name):
    assert yaml_file(yaml_directory, file_name).exists()
